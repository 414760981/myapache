package com.wh;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer {
	
	public void serverStart(int port) {// socket对象。
		try {
			ServerSocket serverSocket = new ServerSocket(port);// 设定socket端口。
			while (true) {
				Socket socket = serverSocket.accept();
				new Processor(socket).start();// 多线程启动，向客户端输出文件流。
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		int port = 801;
		if (args.length == 1) {
			port = Integer.parseInt(args[0]);
		}
		
		new WebServer().serverStart(port);
		
	}

}
